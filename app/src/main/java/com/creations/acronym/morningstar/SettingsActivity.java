package com.creations.acronym.morningstar;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by m.wright on 8/10/16.
 */
public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.registerOnSharedPreferenceChangeListener(this);

    }




    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //TODO connect grade level to notification system
        if(key.equalsIgnoreCase("user_grade")){
            Log.d("Pref","Classification changed to: "+sharedPreferences.getString("user_grade","Unknown"));
        }
        else if(key.equalsIgnoreCase("user_name")){
            Log.d("Pref","User name changed to: "+sharedPreferences.getString("user_name","Unknown"));
        }
        else if(key.equalsIgnoreCase("id_number")){
            Log.d("Pref","User ID changed to:"+sharedPreferences.getString("id_number","000000"));
        }
        else {
            if (sharedPreferences.getBoolean(key, true)) {
                FirebaseMessaging.getInstance().subscribeToTopic(key);
                Log.d("Pref", "Subscribed to " + key);
            }
            else{
                FirebaseMessaging.getInstance().unsubscribeFromTopic(key);
                Log.d("Pref","Unsubscribed to "+key);
            }
        }
    }
}