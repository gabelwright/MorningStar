package com.creations.acronym.morningstar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CollegeVisits extends AppCompatActivity {

    static ArrayList<String> collegeName = new ArrayList<>();
    static ArrayList<Date> collegeDateTime = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_college_visits);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.collegeVisit_progressBar);
        progressBar.setVisibility(View.VISIBLE);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR,24);
        final Date currentDate = cal.getTime();

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseRef = firebaseDatabase.getReference("collegeList");


        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                collegeDateTime.clear();
                collegeName.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    if (currentDate.before(new Date(ds.child("dateTime").getValue(Long.class)))) {
                        collegeName.add(ds.child("college").getValue(String.class));
                        collegeDateTime.add(new Date(ds.child("dateTime").getValue(Long.class)));
                    }
                }
                if(collegeName.size() == 0){
                    collegeName.add("No upcoming college visits");
                }
                ListView listView = (ListView) findViewById(R.id.collegeVisit_listview);
                ArrayAdapter arrayAdapter = new CollegeListAdaptor();
                listView.setAdapter(arrayAdapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ListView listView = (ListView) findViewById(R.id.collegeVisit_listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(CollegeVisits.this)
                        .setTitle("Sign Up")
                        .setMessage("Would you like to sign up for this College Visit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            signUpforVisit(collegeDateTime.get(position));
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
            }
        });

    }

    private void signUpforVisit(final Date date){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final String name = sharedPref.getString("user_name","noname");
        final String idNumber = sharedPref.getString("id_number","-1");
        final String grade = sharedPref.getString("user_grade","-1");

        if(name.equalsIgnoreCase("noname") || name.equalsIgnoreCase("") || grade.equalsIgnoreCase("-1")
                || idNumber.equalsIgnoreCase("-1") || idNumber.equalsIgnoreCase("")){
            new AlertDialog.Builder(CollegeVisits.this)
                    .setTitle("Missing Information")
                    .setMessage("Please enter your name, ID number, and grade in the Settings menu before continuing.")
                    .setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                        }
                    })
                    .setNegativeButton("Cancel",null)
                    .show();


        }else {
            new AlertDialog.Builder(CollegeVisits.this)
                    .setTitle("Please verify your name, ID number, and grade level are correct.")
                    .setMessage(name + "\n" + idNumber + "\n" + grade)
                    .setPositiveButton("Sign Up", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference dataRef = database.getReference("collegeVisits/" + date.getTime() + "/students");
                            Log.d("mgw", "1");
                            Map<String, String> student = new HashMap<>();
                            student.put("name", name);
                            student.put("id", idNumber);
                            student.put("grade", grade);
                            dataRef.push().setValue(student, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        Toast.makeText(getApplicationContext(), "Signed Up!", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error, please try again later", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private class CollegeListAdaptor extends ArrayAdapter<String>{
        public CollegeListAdaptor(){
            super(CollegeVisits.this,R.layout.collegevisits_listview_layout,collegeName);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View itemView = convertView;
            if(convertView == null){
                itemView = getLayoutInflater().inflate(R.layout.collegevisits_listview_layout,parent,false);
            }

            if(collegeName.get(0).equals("No upcoming college visits")){
                TextView textView1 = (TextView) itemView.findViewById(R.id.collegeVisit_layout_name);
                textView1.setText("No upcoming college visits  :(");

                TextView textView2 = (TextView) itemView.findViewById(R.id.collegeVisit_layout_date);
                textView2.setText("");

                TextView textView3 = (TextView) itemView.findViewById(R.id.collegeVisit_layout_time);
                textView3.setText("");

                ListView listView = (ListView) findViewById(R.id.collegeVisit_listview);
                listView.setOnItemClickListener(null);

            }
            else {
                SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d");
                SimpleDateFormat stf = new SimpleDateFormat("h:mm a");

                TextView textView1 = (TextView) itemView.findViewById(R.id.collegeVisit_layout_name);
                textView1.setText(collegeName.get(position));

                TextView textView2 = (TextView) itemView.findViewById(R.id.collegeVisit_layout_date);
                textView2.setText(sdf.format(collegeDateTime.get(position)));

                TextView textView3 = (TextView) itemView.findViewById(R.id.collegeVisit_layout_time);
                textView3.setText(stf.format(collegeDateTime.get(position)));
            }
            return itemView;


        }
    }
}
