package com.creations.acronym.morningstar;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements LocationListener, OnMapReadyCallback {

    private GoogleMap mMap;
    LocationManager locationManager;
    String provider;
    Location location;
    RadioGroup radioGroup;
    boolean locationActive;
    final LatLng coronadoMap = new LatLng(31.83670, -106.545590);
    final LatLng coronadoSchool = new LatLng(31.836791, -106.545856);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        provider = locationManager.getBestProvider(new Criteria(), false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Location permission not granted", Toast.LENGTH_LONG).show();

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 5);
            return;
        }
         else {
            location = locationManager.getLastKnownLocation(provider);
            locationActive = true;

        }
        if (locationManager.getLastKnownLocation(provider) == null) {
            locationActive = false;
            Toast.makeText(this, "Current location not available", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 5) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    provider = locationManager.getBestProvider(new Criteria(), false);
                    location = locationManager.getLastKnownLocation(provider);
                    if(location == null){
                        Toast.makeText(getApplicationContext(),"Current location not available",Toast.LENGTH_LONG).show();
                        locationActive = false;
                    }else {
                        locationActive = true;
                    }
                } catch (SecurityException e) {
                    e.printStackTrace();
                    Log.i("Per","Security Exeption");
                }

            }else{
                Log.i("Permission","Denied");
                locationActive = false;
            }
            return;
        }
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coronadoSchool, 18f));
        if(locationActive && location != null) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())));
        }
        GroundOverlayOptions overlayDown = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.roomdown))
                .position(coronadoMap, 225f);
        mMap.addGroundOverlay(overlayDown);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.downstairsRadio) {
                    mMap.clear();
                    if(locationActive) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())));
                    }
                    GroundOverlayOptions overlayDown = new GroundOverlayOptions()
                            .image(BitmapDescriptorFactory.fromResource(R.drawable.roomdown))
                            .position(coronadoMap, 225f);
                    mMap.addGroundOverlay(overlayDown);

                } else if (checkedId == R.id.upstairsRadio) {
                    mMap.clear();
                    if(locationActive) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())));
                    }
                    GroundOverlayOptions overlayUp = new GroundOverlayOptions()
                            .image(BitmapDescriptorFactory.fromResource(R.drawable.roomup))
                            .position(coronadoMap, 225f);
                    mMap.addGroundOverlay(overlayUp);

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationManager.requestLocationUpdates(provider, 1000, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location newLocation) {
        mMap.clear();
        location = newLocation;
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        if(radioGroup.getCheckedRadioButtonId() == R.id.downstairsRadio){
            GroundOverlayOptions overlayDown = new GroundOverlayOptions()
                    .image(BitmapDescriptorFactory.fromResource(R.drawable.roomdown))
                    .position(coronadoMap, 225f);
            mMap.addGroundOverlay(overlayDown);
        }else{
            mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())));
            GroundOverlayOptions overlayUp = new GroundOverlayOptions()
                    .image(BitmapDescriptorFactory.fromResource(R.drawable.roomup))
                    .position(coronadoMap, 225f);
            mMap.addGroundOverlay(overlayUp);
        }
        mMap.addMarker(new MarkerOptions().position(new LatLng(lat,lng)));


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
