package com.creations.acronym.morningstar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class Conseling extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conseling);
    }

    public void collegeVisits(View view){
        startActivity(new Intent(getApplicationContext(),CollegeVisits.class));
    }

    public void scholar(View view){
        startActivity(new Intent(getApplicationContext(),Scholarships.class));
    }


}
