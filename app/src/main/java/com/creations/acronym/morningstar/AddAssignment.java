package com.creations.acronym.morningstar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.HashSet;

public class AddAssignment extends AppCompatActivity {

    EditText editText;
    int editMode;
    int position;

    public void saveButton(View view){

        editText = (EditText) findViewById(R.id.editText);
        String newAssign = editText.getText().toString();

        if(editMode == 1) {
            DailyPlanner.arrayList.set(position,newAssign);
        }
        else if(editMode == 0){
            DailyPlanner.arrayList.add(newAssign);
            Log.i("Note",DailyPlanner.arrayList.toString());
        }

       if(DailyPlanner.set == null) {

            DailyPlanner.set = new HashSet<String>();
        }
        else{
           DailyPlanner.set.clear();
       }

        DailyPlanner.set.addAll(DailyPlanner.arrayList);
        Log.i("Notes second set",DailyPlanner.set.toString());

        SharedPreferences sharedPref = this.getSharedPreferences("com.creations.acronym.MorningStar", Context.MODE_PRIVATE);
        sharedPref.edit().remove(DailyPlanner.NOTES).apply();
        sharedPref.edit().putStringSet(DailyPlanner.NOTES,DailyPlanner.set).apply();

        DailyPlanner.arrayList.clear();
        DailyPlanner.set.clear();

        Intent i = new Intent(getApplicationContext(), DailyPlanner.class);
        startActivity(i);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_assignment);

        Intent i = getIntent();
        editMode = i.getIntExtra("EditMode",-1);

        if (editMode == 1){
            editText = (EditText) findViewById(R.id.editText);
            position = i.getIntExtra("position",-1);
            editText.setText(DailyPlanner.arrayList.get(position));

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            Log.i("Notes","Navigate back");
            navigateUpTo(new Intent(AddAssignment.this,DailyPlanner.class));
        }
        return true;
    }
}
