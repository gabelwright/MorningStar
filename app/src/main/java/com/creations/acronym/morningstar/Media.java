package com.creations.acronym.morningstar;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public class Media extends AppCompatActivity {

    String ctvURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);

        final long FETCH_TIME = 21600;
        final Button button = (Button) findViewById(R.id.ctv_button2);
        button.setEnabled(false);

        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        remoteConfig.setConfigSettings(configSettings);
        remoteConfig.setDefaults(R.xml.remote_config_values);
        remoteConfig.fetch(FETCH_TIME).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                remoteConfig.activateFetched();
                ctvURL = remoteConfig.getString("ctv_url");
                button.setEnabled(true);
            }
        });
    }

    public void ctv(View view){
        Intent website = new Intent(Intent.ACTION_VIEW, Uri.parse(ctvURL));
        startActivity(website);
    }

    public void explorer(View view){

    }

    public void photoUpload(View view){
        startActivity(new Intent(getApplicationContext(),SpiritPhotos.class));
    }
}
