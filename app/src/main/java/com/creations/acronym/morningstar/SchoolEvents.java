package com.creations.acronym.morningstar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SchoolEvents extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_events);
    }

    public void events(View view){
        startActivity(new Intent(getApplicationContext(),Events.class));
    }

    public void dailyPoll(View view){
        startActivity(new Intent(getApplicationContext(),VotingActivity.class));
    }

    public void planner(View view){
        startActivity(new Intent(getApplicationContext(),DailyPlanner.class));
    }


}
