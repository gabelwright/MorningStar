package com.creations.acronym.morningstar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class PolicyView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_view);

        WebView webView = (WebView) findViewById(R.id.policyWebView);

        assert webView != null;
        webView.loadUrl("http://www.acronymcreations.com/policy.html");
    }
}
