package com.creations.acronym.morningstar;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DailyPlanner extends AppCompatActivity {

    static ArrayList<String> arrayList = new ArrayList<String>();
    static ArrayAdapter<String> arrayAdapter;
    static Set<String> set;
    static final String NOTES = "notes";

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.action_add_note){
            Intent i = new Intent(getApplicationContext(),AddAssignment.class);
            i.putExtra("EditMode",0);
            startActivity(i);
        }
        else if(item.getItemId() == android.R.id.home){
            navigateUpTo(new Intent(this,MainActivity.class));
        }
        else{

        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_planner);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(!arrayList.isEmpty()) {
            arrayList.clear();
        }

        ListView listView = (ListView) findViewById(R.id.listViewPlanner);

        SharedPreferences sharedPref = this.getSharedPreferences("com.creations.acronym.MorningStar", Context.MODE_PRIVATE);

        if(sharedPref.getStringSet(NOTES,null) == null){
            Log.i("Notes","Is this running?");
            sharedPref.edit().putStringSet(NOTES,set).apply();
        }
        else{
            set = sharedPref.getStringSet(NOTES,null);
            arrayList.addAll(set);
        }

        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);
        listView.setAdapter(arrayAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(getApplicationContext(),AddAssignment.class);
                i.putExtra("EditMode",1);
                i.putExtra("position",position);
                startActivity(i);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final int positionPrime = position;

                new AlertDialog.Builder(DailyPlanner.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete?")
                        .setMessage("Are you sure you want to delete this note?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences sharedPref = DailyPlanner.this.getSharedPreferences("com.creations.acronym.MorningStar", Context.MODE_PRIVATE);

                                arrayList.remove(positionPrime);
                                if(sharedPref.getStringSet(NOTES,null) == null){
                                    set = new HashSet<String>();
                                }else{
                                    set.clear();
                                    set.addAll(arrayList);
                                }
                                sharedPref.edit().remove(NOTES).apply();
                                sharedPref.edit().putStringSet(NOTES,set).apply();

                                arrayAdapter.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();

                return true;
            }
        });


    }




}
