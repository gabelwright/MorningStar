package com.creations.acronym.morningstar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class Scholarships extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scholarships);

        WebView webView = (WebView) findViewById(R.id.scholar_webview);
        webView.loadUrl("http://www.acronymcreations.com/tbirdalley/scholarships.html");
    }
}
