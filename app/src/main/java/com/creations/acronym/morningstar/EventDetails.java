package com.creations.acronym.morningstar;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class EventDetails extends AppCompatActivity {

    int id;
    SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d");
    SimpleDateFormat stf = new SimpleDateFormat("h:mm a");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        TextView eventTitle = (TextView) findViewById(R.id.eventTitle);
        TextView eventLocation = (TextView) findViewById(R.id.eventLocation);
        TextView eventTime = (TextView) findViewById(R.id.eventTime);
        TextView eventDate = (TextView) findViewById(R.id.eventDate);

        Intent i = getIntent();
        id = i.getIntExtra("id",-1);

        eventTitle.setText(Events.title.get(id));
        eventLocation.setText(Events.location.get(id));
        eventDate.setText(sdf.format(Events.date.get(id)));
        eventTime.setText(stf.format(Events.date.get(id)));

    }

    public void addToCal(View view){

        Intent i = new Intent(Intent.ACTION_INSERT);
        i.setData(CalendarContract.Events.CONTENT_URI);
        i.setType("vnd.android.cursor.item/event");

        i.putExtra(CalendarContract.Events.TITLE,Events.title.get(id));
        i.putExtra(CalendarContract.Events.EVENT_LOCATION,Events.location.get(id));
        i.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,Events.date.get(id).getTime());


        startActivity(i);

    }
}
