package com.creations.acronym.morningstar;

/**
 * Created by m.wright on 8/1/16.
 */
public class NotificationObject {
    private String title;
    private String message;
    //private Topic topic;
    private String topic;


    public enum Topic {admin,counsel,schoolE,IB}

    public NotificationObject(String title, String message, String topic) {

        this.title = title;
        this.message = message;
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public int getDrawable(){
        return getDrawable(topic);
    }

//    public int getDrawable(Topic topic){
//        switch (topic){
//            case admin:
//                return R.drawable.add_icon;
//            case counsel:
//                return R.drawable.back_icon;
//            case schoolE:
//                return R.drawable.cast_album_art_placeholder;
//            case IB:
//                return R.drawable.cast_ic_mini_controller_pause;
//        }
//        return R.drawable.cast_ic_mini_controller_play;
//    }
    public int getDrawable(String topic){
        switch (topic){
            case "administration":
                return R.drawable.notif_a;
            case "counselor":
                return R.drawable.notif_c;
            case "schoolEvents":
                return R.drawable.notif_e;
            case "ibProgram":
                return R.drawable.notif_ib;
            case "library":
                return R.drawable.notif_l;
        }
        return R.drawable.tbird1;
    }


}
