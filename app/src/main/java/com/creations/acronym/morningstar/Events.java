package com.creations.acronym.morningstar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Events extends AppCompatActivity {

    static ArrayList<String> title = new ArrayList<>();
    static ArrayList<Date> date = new ArrayList<>();
    static ArrayList<String> location = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.events_progressBar);
        progressBar.setVisibility(View.VISIBLE);

        ListView listView = (ListView) findViewById(R.id.listViewEvents);


        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR,-3);
        final Date currentDate = cal.getTime();

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseRef = firebaseDatabase.getReference("events");



        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                title.clear();
                date.clear();
                location.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){

                    if (currentDate.before(new Date(ds.child("dateTime").getValue(Long.class)))) {
                        title.add(ds.child("name").getValue(String.class));
                        location.add(ds.child("location").getValue(String.class));

                        Date date1 = new Date(ds.child("dateTime").getValue(Long.class));
                        date.add(date1);
                    }

                }
                if(title.size() == 0){
                    title.add("No upcoming Events");
                }

                ListView listView = (ListView) findViewById(R.id.listViewEvents);
                ArrayAdapter arrayAdapter = new EventsListAdaptor();
                listView.setAdapter(arrayAdapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ArrayAdapter arrayAdapter = new EventsListAdaptor();
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(Events.this)
                        .setTitle("Add to Calendar?")
                        .setMessage("Would you like to add this event to your calendar?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addToCal(position);
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
            }
        });


    }

    public void addToCal(int id){

        Intent i = new Intent(Intent.ACTION_INSERT);
        i.setData(CalendarContract.Events.CONTENT_URI);
        i.setType("vnd.android.cursor.item/event");

        i.putExtra(CalendarContract.Events.TITLE,Events.title.get(id));
        i.putExtra(CalendarContract.Events.EVENT_LOCATION,Events.location.get(id));
        i.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,Events.date.get(id).getTime());


        startActivity(i);

    }

    private class EventsListAdaptor extends ArrayAdapter<String> {
        public EventsListAdaptor() {
            super(Events.this, R.layout.events_listview_layout, title);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //checks to see if we have a view, if not, creates one from our layout.xml
            View itemView = convertView;
            if (convertView == null) {
                itemView = getLayoutInflater().inflate(R.layout.events_listview_layout, parent, false);
            }

            if(title.get(0).equals("No upcoming Events")){
                TextView eventTitleText = (TextView) itemView.findViewById(R.id.eventTitle);
                eventTitleText.setText("No upcoming Events  :(");

                TextView eventLocationText = (TextView) itemView.findViewById(R.id.eventDate);
                eventLocationText.setText("");

                TextView eventTimeText = (TextView) itemView.findViewById(R.id.eventTime);
                eventTimeText.setText("");

                TextView eventLocation = (TextView) itemView.findViewById(R.id.eventLocation);
                eventLocation.setText("");

                ListView listView = (ListView) findViewById(R.id.listViewEvents);
                listView.setOnItemClickListener(null);
            }
            else {

                SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d");
                SimpleDateFormat stf = new SimpleDateFormat("h:mm a");

                String currentEvent = title.get(position);
                String currentDate = sdf.format(date.get(position));
                String currentTime = stf.format(date.get(position));
                String currentLocation = location.get(position);

                TextView eventTitleText = (TextView) itemView.findViewById(R.id.eventTitle);
                eventTitleText.setText(currentEvent);

                TextView eventLocationText = (TextView) itemView.findViewById(R.id.eventDate);
                eventLocationText.setText(currentDate);

                TextView eventTimeText = (TextView) itemView.findViewById(R.id.eventTime);
                eventTimeText.setText(currentTime);

                TextView eventLocation = (TextView) itemView.findViewById(R.id.eventLocation);
                eventLocation.setText(currentLocation);
            }
            return itemView;
        }
    }

}
