package com.creations.acronym.morningstar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.appcompat.*;
import android.support.v7.appcompat.BuildConfig;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class VotingActivity extends AppCompatActivity {

    static ArrayList<String> arrayList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    static int currentCount;
    static int votingCount;
    static String[] votingOptions;
    static float[] votingResults;
    static float totalVotes;
    final static long FETCH_TIME = 21600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.voting_progressBar);
        progressBar.setVisibility(View.VISIBLE);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        currentCount = sharedPreferences.getInt("voting_count",0);

        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        remoteConfig.setConfigSettings(configSettings);
        remoteConfig.setDefaults(R.xml.remote_config_values);
        remoteConfig.fetch(FETCH_TIME).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    remoteConfig.activateFetched();
                    TextView textView = (TextView) findViewById(R.id.voting_questionText);
                    textView.setText(remoteConfig.getString("voting_question"));
                    votingOptions = remoteConfig.getString("voting_options").split(":");
                    votingCount = (int) remoteConfig.getLong("voting_count");

                    arrayList.clear();
                    for(String string:votingOptions){
                        arrayList.add(string);
                    }
                    if(currentCount<votingCount)
                        setUpVote();
                    else
                        showResults();

                }
                else
                    Toast.makeText(getApplicationContext(),"BUG: "+task.getException().toString(),Toast.LENGTH_LONG).show();
            }
        });



    }

    public void setUpVote(){
        arrayAdapter = new ArrayAdapter<String>(VotingActivity.this,android.R.layout.simple_list_item_single_choice,arrayList);
        ListView listView = (ListView) findViewById(R.id.voting_listView);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setAdapter(arrayAdapter);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.voting_progressBar);
        progressBar.setVisibility(View.GONE);
    }
/*

 */
    private void showResults(){
        final ListView listView = (ListView) findViewById(R.id.voting_listView);
        final ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(votingOptions));
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.voting_progressBar);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dataRef = firebaseDatabase.getReference("currentVote");
        votingResults = new float[votingOptions.length];
        dataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                totalVotes = 0;
                for(int i=0;i<votingOptions.length;i++){
                    if(!dataSnapshot.hasChild(votingOptions[i])){
                        votingResults[i] = 0;
                    }
                    else {
                        votingResults[i] = (float) dataSnapshot.child(votingOptions[i]).getValue(Long.class);
                        totalVotes += votingResults[i];
                    }
                }
                ArrayAdapter arrayAdapter1 = new VotingResultsArrayAdaptor(getApplicationContext(),arrayList);
                assert listView != null;
                listView.setAdapter(arrayAdapter1);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
        Button button = (Button) findViewById(R.id.voting_castVote);
        assert button != null;
        button.setClickable(false);
        button.setVisibility(Button.INVISIBLE);




    }

    public void castVote(View view){

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.voting_progressBar);
        progressBar.setVisibility(View.VISIBLE);

        final ListView listView = (ListView) findViewById(R.id.voting_listView);
        if(listView.getCheckedItemCount() != 1){
            Toast.makeText(getApplicationContext(),"You did not make a selection",Toast.LENGTH_LONG).show();
        }
        else{

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference dataRef = firebaseDatabase.getReference("currentVote");
            dataRef.runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {
                    if(!mutableData.hasChild(votingOptions[listView.getCheckedItemPosition()])){
                        Log.d("vote","data is null");
                        mutableData.child(votingOptions[listView.getCheckedItemPosition()]).setValue(1);
                        return Transaction.success(mutableData);
                    }else {
                        Log.d("vote","data is not null");
                        long count = mutableData.child(votingOptions[listView.getCheckedItemPosition()]).getValue(Long.class);
                        Log.d("vote count", count + "");
                        count++;
                        mutableData.child(votingOptions[listView.getCheckedItemPosition()]).setValue(count);
                        return Transaction.success(mutableData);
                    }
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                    if(databaseError == null) {
                        Log.d("vote", "No error");
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(VotingActivity.this);
                        sharedPreferences.edit().putInt("voting_count",votingCount).apply();
                        showResults();
                    }
                    else {
                        Log.d("vote", "error: " + databaseError.getMessage());
                        Log.d("vote error code",databaseError.getCode()+"");

                        if(databaseError.getCode() == -4){
                            View view1 = new View(getApplicationContext());
                            castVote(view1);
                        }
                    }
                }
            });
        }
    }

    private class VotingResultsArrayAdaptor extends ArrayAdapter<String>{
        public VotingResultsArrayAdaptor(Context context,ArrayList<String> options){
            super(context,0,options);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View itemView = convertView;
            if(itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.voting_listview_layout,parent,false);
            }

            TextView option = (TextView) itemView.findViewById(R.id.votingLayoutOption);
            TextView percent = (TextView) itemView.findViewById(R.id.votingLayoutPercent);
            ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);

            option.setText(votingOptions[position]);
            float percentShown = (votingResults[position]/totalVotes) * 100;
            percent.setText(String.format("%.0f",percentShown)+"%");
            progressBar.setProgress((int) percentShown);

            return itemView;
        }

    }


















}
