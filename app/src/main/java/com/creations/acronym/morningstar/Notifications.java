package com.creations.acronym.morningstar;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Notifications extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.notif_progressBar);
        progressBar.setVisibility(View.VISIBLE);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference dataRef = firebaseDatabase.getReference("notifications");
        final ArrayList<NotificationObject> notes = new ArrayList<>();
        dataRef.orderByValue();


        dataRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                notes.clear();


                for(DataSnapshot ds : dataSnapshot.getChildren()){


                    notes.add(new NotificationObject(ds.child("title").getValue(String.class),
                            ds.child("message").getValue(String.class),
                            ds.child("topic").getValue(String.class)));

                }
                ListView listView = (ListView) findViewById(R.id.notif_listview);
                ArrayAdapter<NotificationObject> arrayAdapter = new NotificationsArrayAdaptor(getApplicationContext(),notes);
                listView.setAdapter(arrayAdapter);
                progressBar.setVisibility(View.GONE);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        new AlertDialog.Builder(Notifications.this)
                                .setIcon(notes.get(position).getDrawable())
                                .setTitle(notes.get(position).getTitle())
                                .setMessage(notes.get(position).getMessage())
                                .setPositiveButton("OK",null)
                                .show();
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private class NotificationsArrayAdaptor extends ArrayAdapter<NotificationObject>{
        public NotificationsArrayAdaptor(Context context,ArrayList<NotificationObject> notes){
            super(context,0,notes);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if(convertView == null)
                itemView = getLayoutInflater().inflate(R.layout.notif_listview_layout,parent,false);

            TextView title = (TextView) itemView.findViewById(R.id.notif_title);
            TextView message = (TextView) itemView.findViewById(R.id.notif_message);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.notif_image);

            NotificationObject note = getItem(position);

            title.setText(note.getTitle());

            message.setText(note.getMessage());
            imageView.setImageResource(note.getDrawable());

            return itemView;
        }
    }
}


