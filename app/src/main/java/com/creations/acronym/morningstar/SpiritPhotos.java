package com.creations.acronym.morningstar;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;

import static com.creations.acronym.morningstar.R.id.imageView;

public class SpiritPhotos extends AppCompatActivity {

    final int INTENT_REQUEST_CODE = 1;
    final int PERMISSION_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spirit_photos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.photo_progress2);
        progressBar.setVisibility(View.GONE);

        beforeSelection();

        Button button = (Button) findViewById(R.id.photo_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getName();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.upload);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("mgw","1");
                int permissionCheck = ActivityCompat.checkSelfPermission(SpiritPhotos.this, Manifest.permission.READ_EXTERNAL_STORAGE);

                if(permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    photoPicker();
                }else if(permissionCheck == PackageManager.PERMISSION_DENIED){
                    Log.d("mgw","2");
                    boolean bo = ActivityCompat.shouldShowRequestPermissionRationale(SpiritPhotos.this, Manifest.permission.READ_EXTERNAL_STORAGE);
                    ActivityCompat.requestPermissions(SpiritPhotos.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    Log.d("mgw","2"+bo);
                }

            }
        });
    }

    private void photoPicker(){

        Intent picker = new Intent(Intent.ACTION_PICK);
        picker.setType("image/*");
        startActivityForResult(picker,INTENT_REQUEST_CODE);
    }

    private void beforeSelection(){
        TextView textView = (TextView) findViewById(R.id.photoUpload_Title);
        TextView textView1 = (TextView) findViewById(R.id.photo_body);
        Button button = (Button) findViewById(R.id.photo_button);
        ImageView imageView = (ImageView) findViewById(R.id.photoUpload_imageView);

        textView.setVisibility(View.VISIBLE);
        textView1.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
        button.setVisibility(View.GONE);
    }

    private void afterSelection(){
        ImageView imageView = (ImageView) findViewById(R.id.photoUpload_imageView);
        imageView.setImageBitmap(null);
        TextView textView = (TextView) findViewById(R.id.photoUpload_Title);
        TextView textView1 = (TextView) findViewById(R.id.photo_body);
        Button button = (Button) findViewById(R.id.photo_button);

        textView.setVisibility(View.GONE);
        textView1.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        button.setVisibility(View.VISIBLE);
    }

    private void getName(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        final String name = sharedPref.getString("user_name","noname");
        final String idNumber = sharedPref.getString("id_number","-1");

        if(name.equalsIgnoreCase("noname") || name.equalsIgnoreCase("")
                || idNumber.equalsIgnoreCase("-1") || idNumber.equalsIgnoreCase("")){
            new AlertDialog.Builder(SpiritPhotos.this)
                    .setTitle("Missing Information")
                    .setMessage("Please enter your name, ID number, and grade in the Settings menu before continuing.")
                    .setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                        }
                    })
                    .setNegativeButton("Cancel",null)
                    .show();


        }else{
            new AlertDialog.Builder(SpiritPhotos.this)
                    .setTitle("Confirm")
                    .setMessage("Confirm your name and ID number:\n\n"+name+"\n"+idNumber)
                    .setNegativeButton("Change Info", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                        }
                    })
                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            uploadPhoto(name,idNumber);
                        }
                    }).show();
        }
    }

    private void uploadPhoto(String name,String id){
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.photo_progress2);
        progressBar.setVisibility(View.VISIBLE);
        Calendar cal = Calendar.getInstance();
        final java.util.Date currentDate = cal.getTime();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storRef = storage.getReferenceFromUrl("gs://tbird-alley.appspot.com/").child("SpiritPhotos").child(name+"_"+id+"/")
                .child(currentDate.toString().replaceAll(" ","").replaceAll(":",""));
        ImageView imageView = (ImageView) findViewById(R.id.photoUpload_imageView);
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("image/jpeg")
                .build();
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = storRef.putBytes(data,metadata);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getApplicationContext(),"Photo Uploaded! Thanks for contributing",Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                beforeSelection();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(),"Upload Failed, Please try again later.",Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }
        });


        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                float progress = (100*taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());

                progressBar.setProgress((int) progress);
            }
        });


    }





    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d("mgw","3");
        if(requestCode == PERMISSION_REQUEST_CODE){
            Log.d("mgw","4");
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                photoPicker();
            }else{
                Toast.makeText(getApplicationContext(),"Unable to access your gallery. Please change your permission settings to allow access.",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturned){
        super.onActivityResult(requestCode,resultCode,imageReturned);

        if(requestCode == INTENT_REQUEST_CODE){
            if(resultCode==RESULT_OK){
                try{
                    afterSelection();
                    final Uri imageUri = imageReturned.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ImageView imageView = (ImageView) findViewById(R.id.photoUpload_imageView);
                    imageView.setImageBitmap(selectedImage);



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
