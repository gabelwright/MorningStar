package com.creations.acronym.morningstar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class Announce extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announce);

        WebView webView = (WebView) findViewById(R.id.webView);

        
        assert webView != null;
        webView.loadUrl("http://www.acronymcreations.com/announcements.html");
    }
}
