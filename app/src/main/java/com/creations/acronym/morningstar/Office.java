package com.creations.acronym.morningstar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Office extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_office);

    }

    public void announcements(View view){
        startActivity(new Intent(getApplicationContext(),Announce.class));
    }

    public void dressCode(View view){
        startActivity(new Intent(getApplicationContext(),PolicyView.class));
    }

    public void notifications(View view){
        startActivity(new Intent(getApplicationContext(),Notifications.class));
    }

    public void schoolMap(View view){
        startActivity(new Intent(getApplicationContext(),MapsActivity.class));
    }
}
