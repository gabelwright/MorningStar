package com.creations.acronym.morningstar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity {

    public void whatsUp(View view){
        startActivity(new Intent(getApplicationContext(),Office.class));
    }

    public void media(View view){
        startActivity(new Intent(getApplicationContext(),Media.class));
    }

    public void counsel(View view){
        startActivity(new Intent(getApplicationContext(),Conseling.class));
    }

    public void schoolEvents(View view){
        startActivity(new Intent(getApplicationContext(),SchoolEvents.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(),"Could not authenticate with server. Please try again later.",Toast.LENGTH_LONG).show();

            }
        });

        if(FirebaseInstanceId.getInstance().getToken() != null)
            Log.d("Token",FirebaseInstanceId.getInstance().getToken());


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if(sharedPreferences.getBoolean("firstTime", true)){
            FirebaseMessaging fb = FirebaseMessaging.getInstance();
            fb.subscribeToTopic("administration");
            fb.subscribeToTopic("counselor");
            fb.subscribeToTopic("schoolEvents");
            Log.d("topics","Subscribed");
            sharedPreferences.edit().putBoolean("firstTime",false).apply();
            Log.d("Topic",sharedPreferences.getBoolean("firstTime",true)+"");
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_feedback) {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_email)
                    .setTitle("Have Feedback?")
                    .setMessage("If you have ideas of how this app could be improved, please email them to ACRONYMcreations@gmail.com")
                    .setPositiveButton("OK",null)
                    .show();


        }
        else if(id == R.id.action_settings)
            startActivity(new Intent(getApplicationContext(),SettingsActivity.class));
        //TODO When user clicks on notification, open app to notification page.

        return super.onOptionsItemSelected(item);
    }


}
